#include "easy_poly.h"

#include <algorithm>
#include <stddef.h>

namespace image_control
{
    bool EasyPoly::PolyPoint::operator==( const PolyPoint& rhs ) const
    { return ( x == rhs.x ) && ( y == rhs.y ); }

    EasyPoly::PolyPoint& EasyPoly::PolyPoint::operator-=( const PolyPoint& rhs )
    {
        x -= rhs.x;
        y -= rhs.y;
        return *this;
    }

    bool EasyPoly::PolyRect::inside( const PolyPoint& p ) const
    {
        return  ( p.x >= top_left.x ) && ( p.x <= bottom_right.x )
            &&  ( p.y >= top_left.y ) && ( p.y <= bottom_right.y )
            ;
    }

    bool EasyPoly::PolyRect::inside( int x, int y ) const
    { return inside( { x, y } ); }

    EasyPoly::EasyPoly()
    : mCached( false )
    , mBoundingWidth( 0 )
    , mBoundingHeight( 0 )
    {
        mBoundingRect.top_left.x = 0;
        mBoundingRect.top_left.y = 0;
        mBoundingRect.bottom_right.x = 0;
        mBoundingRect.bottom_right.y = 0;
    }

    void EasyPoly::addPoint( const PolyPoint& p )
    {
        // Check if point already exists
        for( size_t i = 0u; i<mPoints.size(); ++i )
        {
            if( mPoints[i] == p )
            { return; }
        }

        if( mPoints.empty() )
        {
            mBoundingRect.top_left = p;
            mBoundingRect.bottom_right = p;
        }
        mPoints.push_back( p );

        // Update bounding rect
        mBoundingRect.top_left.x = std::min( mBoundingRect.top_left.x, p.x );
        mBoundingRect.top_left.y = std::min( mBoundingRect.top_left.y, p.y );
        mBoundingRect.bottom_right.x = std::max( mBoundingRect.bottom_right.x, p.x );
        mBoundingRect.bottom_right.y = std::max( mBoundingRect.bottom_right.y, p.y );
        mBoundingWidth = ( mBoundingRect.bottom_right.x - mBoundingRect.top_left.x ) + 1;
        mBoundingHeight = ( mBoundingRect.bottom_right.y - mBoundingRect.top_left.y ) + 1;

        // Invalidate cached map
        mCached = false;
    }

    void EasyPoly::addPoint( int x, int y )
    {
        PolyPoint p;
        p.x = x;
        p.y = y;
        addPoint( p );
    }

    const std::vector<EasyPoly::PolyPoint>& EasyPoly::points() const
    { return mPoints; }

    EasyPoly::PointType EasyPoly::checkPoint( const PolyPoint& p ) const
    {
        if( mPoints.empty() )
        { return PointType::OUTSIDE; }


        if( !mBoundingRect.inside( p ) )
        { return PointType::OUTSIDE; }

        if( mCached )
        {
            const PolyPoint& top_left = mBoundingRect.top_left;
            return mPointMap[ static_cast<size_t>( ( p.y - top_left.y ) * mBoundingWidth + ( p.x - top_left.x ) ) ];
        }

        // Ray casting
        // Iterate through the lines of the polygon and count intersections
        size_t intersection_count = 0u;

        for( size_t i = 0u, size = mPoints.size(); i < size; ++i )
        {
            const PolyPoint& p1 = mPoints[i];
            const PolyPoint& p2 = mPoints[ (i+1) % size ];

            const PolyPoint& pstart = p1.y < p2.y ? p1 : p2;
            const PolyPoint& pend = p2.y > p1.y ? p2 : p1;

            if( p == pstart || p == pend )
            { return PointType::ONEDGE; }

            if( ( p.y >= std::min( pstart.y, pend.y ) ) && ( p.y <= std::max( pstart.y, pend.y ) ) )
            {
                if( pstart.y == pend.y )
                {
                    if( ( p.x >= std::min( pstart.x, pend.x ) ) && ( p.x <= std::max( pstart.x, pend.x ) ) )
                    { return PointType::ONEDGE; }
                    continue;
                }

                double pslope = ( pstart.x - pend.x ) / double( pstart.y - pend.y );
                double cslope = ( pstart.x - p.x ) / double( pstart.y - p.y );

                if( pslope == cslope )
                { return PointType::ONEDGE; }
                else if( pslope > cslope )
                { ++intersection_count; }
            }
        }

        if( ( intersection_count % 2u ) == 0u )
        { return PointType::OUTSIDE; }
        else
        { return PointType::INSIDE; }
    }

    EasyPoly::PointType EasyPoly::checkPoint( int x, int y ) const
    { return checkPoint( { x, y } ); }

    const EasyPoly::PolyRect& EasyPoly::boundingRect() const
    { return mBoundingRect; }

    void EasyPoly::buildCache()
    {
        if( !mCached )
        {
            PolyPoint& top_left = mBoundingRect.top_left;
            PolyPoint& bottom_right = mBoundingRect.bottom_right;

            size_t idx = 0;
            int width = std::abs( bottom_right.x - top_left.x ) + 1;
            int height = std::abs( bottom_right.y - top_left.y ) + 1;
            mPointMap.resize( static_cast<size_t>( width * height ) );
            for( int x = top_left.x; x <= bottom_right.x; ++x )
            {
                for( int y = top_left.y; y <= bottom_right.y; ++y )
                {
                    idx = static_cast<size_t>( ( y - top_left.y ) * width + ( x - top_left.x ) );
                    mPointMap[idx] = checkPoint( x, y );
                }
            }
            mCached = true;
        }
    }

    bool EasyPoly::empty() const
    { return mPoints.empty(); }

    bool EasyPoly::operator==( const EasyPoly& rhs ) const
    { return ( mPoints == rhs.mPoints ); }
}
