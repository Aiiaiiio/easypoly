#ifndef EASY_POLY_H
#define EASY_POLY_H

#include <vector>

namespace image_control
{
    // A standalone polygon class

    class EasyPoly
    {
        public:
            struct PolyPoint
            {
                int x, y;

                bool operator==( const PolyPoint& rhs ) const;
                PolyPoint& operator-=( const PolyPoint& rhs );
            };

            struct PolyRect
            {
                PolyPoint top_left, bottom_right;

                bool inside( const PolyPoint& p ) const;
                bool inside( int x, int y ) const;
            };

            enum class PointType
            {
                OUTSIDE
            ,   INSIDE
            ,   ONEDGE
            };

            EasyPoly();

            void addPoint( const PolyPoint& p );
            void addPoint( int x, int y );
            const std::vector<PolyPoint>& points() const;
            PointType checkPoint( const PolyPoint& p ) const;
            PointType checkPoint( int x, int y ) const;

            /// \brief Returns the bounding box of the polygon's points.
            /// \return The bounding box of the polygon's points.
            ///
            const PolyRect& boundingRect() const;

            /// \brief Calculates all the points of the bounding box and stores the results in a lookup map.
            ///
            /// By calculating I mean weather the given point is inside / is on the edge of a segment / outside of the polygon.
            /// This method is costly, useful when points will be checked multiple times, or lengthy calculation is not favourable at the time of checking.
            ///
            void buildCache();

            /// \brief Tells if the polygon is empty (has no points)
            /// \return True if the polygon is empty, false otherwise.
            ///
            bool empty() const;

            /// \brief Tells if rhs is the same polygon as this.
            /// \param The polygon to be compared with this one.
            /// \return True if rhs has the same points, in the same order as this one, false otherwise.
            ///
            bool operator==( const EasyPoly& rhs ) const;

        private:
            bool                            mCached;
            std::vector<PolyPoint>          mPoints;
            std::vector<PointType>          mPointMap;
            PolyRect                        mBoundingRect;
            int                             mBoundingWidth;
            int                             mBoundingHeight;
    };
}

#endif // EASY_POLY_H
