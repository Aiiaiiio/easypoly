#include "main_widget.h"

#include <map>

#include <QMouseEvent>
#include <QPainter>
#include <QColor>

#include "runtime_meas.h"

MainWidget::MainWidget( QWidget* parent )
:   QWidget( parent )
,   mCurrentPolygonIndex( -1 )
,   mCalculationTime( 0 )
,   mRenderedImage( 500, 500, QImage::Format_RGB32 )
,   mLayout( new QVBoxLayout( this ) )
,   mInfoLabel( new QLabel( "", this ) )
,   mImageWidget( new ImageWidget( QImage(), this ) )
{
    setWindowTitle( "EasyPoly visualization" );

    mLayout->addWidget( mInfoLabel, 1 );
    mLayout->addWidget( mImageWidget, 10 );
    mImageWidget->setTransformationMode( Qt::FastTransformation );
    resize( 800, 600 );

    generatePolygonList();
}

MainWidget::~MainWidget()
{}

void MainWidget::mouseReleaseEvent( QMouseEvent* /*event*/ )
{
    if( !mPolygonList.empty() )
    {
        mCurrentPolygonIndex = ( mCurrentPolygonIndex + 1 ) % mPolygonList.size();
        generateImage( mPolygonList[ mCurrentPolygonIndex ] );
        mRenderedImage.save( "a.png" );
        mImageWidget->setImage( mRenderedImage );

        mInfoLabel->setText(
                    QString(
                        "Polygon index: %1  |  Calculated in: %2 ms\n"
                        "Px count: %3  inside: %4  onedge: %5  outside: %6  not outside: %7  undefined: %8"
                    )
                    .arg( mCurrentPolygonIndex )
                    .arg( mCalculationTime/1000000 )
                    .arg( mPxCnt )
                    .arg( mPxInside )
                    .arg( mPxOnEdge )
                    .arg( mPxOutside )
                    .arg( mPxNotOutside )
                    .arg( mPxUndefined )
        );
    }
}

void MainWidget::generatePolygonList()
{
    mPolygonList.clear();
    {
        auto& p = mPolygonList.emplace_back();
        p.addPoint( 50, 50 );
        p.addPoint( 450, 50 );
        p.addPoint( 450, 450 );
        p.addPoint( 50, 450 );
    }

    {
        auto& p = mPolygonList.emplace_back();
        p.addPoint( 100, 100 );
        p.addPoint( 400, 400 );
        p.addPoint( 100, 400 );
    }

    {
        auto& p = mPolygonList.emplace_back();
        p.addPoint( 100, 400 );
        p.addPoint( 250, 100 );
        p.addPoint( 400, 400 );
        p.addPoint( 350, 400 );
        p.addPoint( 250, 150 );
        p.addPoint( 150, 400 );
    }

    {
        auto& p = mPolygonList.emplace_back();
        p.addPoint( 100, 100 );
        p.addPoint( 400, 400 );
        p.addPoint( 400, 100 );
        p.addPoint( 100, 400 );
    }
}

void MainWidget::generateImage( const EasyPoly& p )
{
    QPainter painter( &mRenderedImage );

    static std::map<EasyPoly::PointType, QColor> point_type_to_color_map;
    if( point_type_to_color_map.empty() )
    {
        point_type_to_color_map[ EasyPoly::PointType::INSIDE ] = QColor( 90, 90, 90 );
        point_type_to_color_map[ EasyPoly::PointType::ONEDGE ] = QColor( 0, 255, 0 );
        point_type_to_color_map[ EasyPoly::PointType::OUTSIDE ] = QColor( 0, 0, 0 );
    }

    mPxCnt = mRenderedImage.width() * mRenderedImage.height();
    mPxInside = 0u;
    mPxOnEdge = 0u;
    mPxOutside = 0u;
    mPxNotOutside = 0u;
    mPxUndefined = 0u;

    { RuntimeMeas m( "" );
        for( int row = 0, height = mRenderedImage.height(); row < height; ++row )
        {
            for( int col = 0, width = mRenderedImage.width(); col < width; ++col )
            {
                auto point_type = p.checkPoint( col, row );
                switch( point_type )
                {
                    case EasyPoly::PointType::OUTSIDE: ++mPxOutside; break;
                    case EasyPoly::PointType::INSIDE: ++mPxInside; ++mPxNotOutside; break;
                    case EasyPoly::PointType::ONEDGE: ++mPxOnEdge; ++mPxNotOutside; break;
                    default: ++mPxUndefined; break;
                }
                painter.setPen( point_type_to_color_map[ point_type ] );
                painter.drawPoint( col, row );
            }
        }
        mCalculationTime = m.elapsed();
    }

    painter.setPen( QColor( 100, 100, 255 ) );
    for( size_t i = 0u, size = p.points().size(); i < size; ++i )
    {
        auto& pt = p.points()[i];
        painter.drawEllipse( pt.x - 3, pt.y - 3, 6, 6 );
        painter.drawText( pt.x -10 , pt.y - 10, QString::number( i + 1 ) );
    }
}
