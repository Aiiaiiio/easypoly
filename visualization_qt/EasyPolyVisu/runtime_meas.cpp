#include "runtime_meas.h"

#define ENABLE_RUNTIME_MEAS
#ifdef ENABLE_RUNTIME_MEAS

#include <cstdio>

RuntimeMeas::RuntimeMeas( const char* name )
:   mName( name )
{ mStart = std::chrono::steady_clock::now(); }

RuntimeMeas::~RuntimeMeas()
{ print(); }

int64_t RuntimeMeas::elapsed() const
{ return ( std::chrono::steady_clock::now() - mStart ).count(); }

void RuntimeMeas::print()
{
    auto elapsed_nano = elapsed();
    fprintf( stderr, "[%s] %.2f ms\n", mName.c_str(), elapsed_nano / 1000000.0 ); fflush( stderr );
}

#else

RuntimeMeas::RuntimeMeas( const char* )
{}

RuntimeMeas::~RuntimeMeas()
{}

int64_t RuntimeMeas::elapsed() const
{ return 0; }

void RuntimeMeas::print()
{}

#endif
