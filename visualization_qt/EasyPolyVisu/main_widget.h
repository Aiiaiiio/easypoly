#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include <QImage>
#include <QLabel>
#include <QVBoxLayout>
#include <QList>

#include "image_widget.h"
#include "../../src/easy_poly.h"

class MainWidget : public QWidget
{
    Q_OBJECT

    public:
        typedef image_control::EasyPoly EasyPoly;

        MainWidget( QWidget* parent = nullptr );
        ~MainWidget();

    protected:
            void mouseReleaseEvent(QMouseEvent *event) override;

    private:
        void generatePolygonList();
        void generateImage( const EasyPoly& p );

        QList<EasyPoly>     mPolygonList;
        int                 mCurrentPolygonIndex;
        int64_t             mCalculationTime;
        QImage              mRenderedImage;
        QVBoxLayout*        mLayout;
        QLabel*             mInfoLabel;
        ImageWidget*        mImageWidget;
        size_t              mPxCnt;
        size_t              mPxInside;
        size_t              mPxOutside;
        size_t              mPxOnEdge;
        size_t              mPxNotOutside;
        size_t              mPxUndefined;
};
#endif // MAINWIDGET_H
