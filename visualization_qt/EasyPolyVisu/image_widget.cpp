#include "image_widget.h"

#include <QPainter>
#include <QDragEnterEvent>

ImageWidget::ImageWidget( QImage image, QWidget *parent, Qt::WindowFlags flags )
: QWidget( parent, flags )
, mImage( image )
, mTransformationMode( Qt::SmoothTransformation )
, mForceRecalculation( true )
, mBackGroundColor( qRgb( 210, 210, 210 ) )
, mFillBackground( true )
{}

QImage ImageWidget::image() const
{ return mImage; }

QImage& ImageWidget::image()
{ return mImage; }

void ImageWidget::setImage( const QImage& image )
{
    mImage = image;
    mForceRecalculation = true;
    repaint();
    emit imageChanged( mImage );
}

Qt::TransformationMode ImageWidget::transformationMode() const
{ return mTransformationMode; }

void ImageWidget::setTransformationMode( Qt::TransformationMode transformationMode )
{
    if( transformationMode != mTransformationMode )
    {
        mTransformationMode = transformationMode;
        repaint();
    }
}

void ImageWidget::setBackgroundColor( QRgb color )
{ mBackGroundColor = color; }

void ImageWidget::setFillBackground( bool fill_background )
{
    if( fill_background != mFillBackground )
    {
        mFillBackground = fill_background;
        repaint();
    }
}

QRgb ImageWidget::backgroundColor() const
{ return mBackGroundColor; }

bool ImageWidget::fillBackground() const
{ return mFillBackground; }

QPoint ImageWidget::mapToImage( const QPoint& p ) const
{
    if( mImage.isNull() || !mImageRect.contains( p ) )
    {
        return QPoint();
    }
    else
    {
        QPoint translated_p = p - mImageRect.topLeft();
        QPointF factors(
                    qreal( mImageRect.width() ) / mImage.width()
                ,   qreal( mImageRect.height() ) / mImage.height()
                );

        QPoint mapped_point(
                   static_cast<int>( ( translated_p.x() / factors.x() ) + 0.5 )
               ,   static_cast<int>( ( translated_p.y() / factors.y() ) + 0.5 )
               );
        return mapped_point;
    }
}

QPoint ImageWidget::mapImagePointToWidgetPoint( const QPoint& p ) const
{
    QPointF factors(
                qreal( mImageRect.width() ) / mImage.width()
            ,   qreal( mImageRect.height() ) / mImage.height()
            );

    return QPoint( static_cast<int>( p.x() * factors.x() )
                 , static_cast<int>( p.y() * factors.y() )
                 ) + mImageRect.topLeft();
}

void ImageWidget::paintEvent( QPaintEvent* /*event*/ )
{
    QPainter p( this );
    if( !mImage.isNull() )
    {
        QPointF resize_factors( width() / static_cast<qreal>( mImage.width() )
                              , height() / static_cast<qreal>( mImage.height() )
                              );

        if( mForceRecalculation || ( resize_factors != mResizeFactors ) )
        {
            mResizeFactors = resize_factors;
            mScaledImage = ( resize_factors.x() < resize_factors.y() ) ? mImage.scaledToWidth( width(), mTransformationMode ) : mImage.scaledToHeight( height(), mTransformationMode );
            mForceRecalculation = false;
        }

        if( mScaledImage.hasAlphaChannel() && mFillBackground )
        { p.fillRect( rect(), QBrush( QColor( qRed( mBackGroundColor ), qGreen( mBackGroundColor ), qBlue( mBackGroundColor ) ) ) ); }
        p.drawImage( ( width() - mScaledImage.width() ) / 2, ( height() - mScaledImage.height() ) / 2, mScaledImage );
        mImageRect = QRect(
                         ( width() - mScaledImage.width() ) / 2
                     ,   ( height() - mScaledImage.height() ) / 2
                     ,   mScaledImage.width()
                     ,   mScaledImage.height()
                     );
    }

    auto pen = p.pen();
    pen.setColor( QColor( 180, 180, 180 ) );
    p.setPen( pen );
    p.drawRect( 0, 0, width()-1, height()-1 );
}

void ImageWidget::dropEvent( QDropEvent* event )
{ emit dropEventSignal( event ); }

void ImageWidget::dragEnterEvent( QDragEnterEvent* event )
{ event->accept(); }
