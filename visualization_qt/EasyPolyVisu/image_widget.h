#ifndef IMAGE_WIDGET_H
#define IMAGE_WIDGET_H

#include <QWidget>
#include <QImage>
#include <QPointF>

class ImageWidget : public QWidget
{
    Q_OBJECT

    public:
        ImageWidget( QImage image, QWidget *parent = nullptr, Qt::WindowFlags flags = Qt::Widget );
        QImage image() const;
        QImage& image();
        void setImage( const QImage& image );
        Qt::TransformationMode transformationMode() const;
        void setTransformationMode( Qt::TransformationMode transformationMode );
        void setBackgroundColor( QRgb color );
        void setFillBackground( bool fill_background );
        QRgb backgroundColor() const;
        bool fillBackground() const;
        QPoint mapToImage( const QPoint& p ) const;
        QPoint mapImagePointToWidgetPoint( const QPoint& p ) const;

    signals:
        void imageChanged( QImage image );
        void dropEventSignal( QDropEvent* event );

    protected:
        void paintEvent( QPaintEvent* event ) override;
        void dropEvent( QDropEvent* event ) override;
        void dragEnterEvent( QDragEnterEvent* event ) override;

    private:
        QImage                  mImage;
        QImage                  mScaledImage;
        QPointF                 mResizeFactors;
        Qt::TransformationMode  mTransformationMode;
        bool                    mForceRecalculation;
        QRgb                    mBackGroundColor;
        bool                    mFillBackground;
        QRect                   mImageRect;
};

#endif // IMAGE_WIDGET_H
