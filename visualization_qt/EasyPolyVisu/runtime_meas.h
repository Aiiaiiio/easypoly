#ifndef RUNTIME_MEAS_H
#define RUNTIME_MEAS_H

#include <chrono>
#include <string>
#include <stdint.h>

class RuntimeMeas
{
    public:
        RuntimeMeas( const char* name );
        ~RuntimeMeas();
        int64_t elapsed() const;
        void print();

    private:
        std::chrono::time_point<std::chrono::steady_clock>      mStart;
        std::string                                             mName;
};

#endif // RUNTIME_MEAS_H
