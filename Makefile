BUILD_DIR=build
CXX=g++
CXX_FLAGS_RELEASE=--std=c++17 -O3
CXX_FLAGS_DEBUG=--std=c++17 -Og
AR=ar
MAKE=make
MKDIR=mkdir
COPY=cp

.DEFAULT_GOAL := static_lib

$(BUILD_DIR)/objects/easy_poly.o: src/easy_poly.h src/easy_poly.cpp
	@$(MKDIR) -p $(BUILD_DIR)/objects
	@echo -n "src/easy_poly.cpp --> $(BUILD_DIR)/objects/easy_poly.o"
	@if $(CXX) -c src/easy_poly.cpp -o $(BUILD_DIR)/objects/easy_poly.o ; then \
		echo "    Ok"; \
	else \
		echo "    Fail"; \
	fi

$(BUILD_DIR)/lib/easy_poly.a: $(BUILD_DIR)/objects/easy_poly.o
	@$(MKDIR) -p $(BUILD_DIR)/lib
	@echo -n "$(BUILD_DIR)/objects/easy_poly.o --> $(BUILD_DIR)/lib/easy_poly.a"
	@$(AR) rvs $(BUILD_DIR)/lib/easy_poly.a $(BUILD_DIR)/objects/easy_poly.o > /dev/null
	

.PHONY: headers
headers: src/easy_poly.h
	@$(MKDIR) -p $(BUILD_DIR)/include
	@$(COPY) src/easy_poly.h $(BUILD_DIR)/include

.PHONY: static_lib
static_lib: $(BUILD_DIR)/lib/easy_poly.a headers
	@echo "Static lib done"

.PHONE: clean
clean:
	@rm -rf $(BUILD_DIR)
